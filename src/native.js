import tips from './tips.js'
class NativeToH5 {
    constructor(agreement, callback) {
        //方法id
        this._seq = 1;
        //未知有地方在用
        this.mIsNewProtocol = null;
        //协议前缀
        this.agreement = agreement || 'ischool';
        //原生监听方法
        this.callback = callback || 'onShouxinerSchemeCallback';
    }

    //增加协议对象
    _pfnSet(fn, callback) {
        this[fn] = callback
    }

    //获取协议对象
    _pfnGet(fn) {
        return this[fn]
    }

    //删除协议对象
    _pfnRemove(fn) {
        delete this[fn]
    }

    //调用客户端协议
    invoke(type, action, seqNum, data) {
        //var uri = "ischool://" + type + "/" + action + "?cmdSeq=" + seqNum;
        var uri = `${this.agreement}://${type}/${action}?cmdSeq=${seqNum}`;
        if (data) {
            uri += "&" + this.jsonToUrlEncode(data);
        }
        window.location = uri
    }

    //传参格式化
    jsonToUrlEncode(data) {
        var str = "";
        if (data) {
            for (var item in data) {
                if (str.length > 0) {
                    str += "&"
                }
                str += (item + '=' + encodeURIComponent(data[item]));
            }
        }
        return str
    }

    //获取唯一id
    generateCmdSeq() {
        return this._seq++
    }

    //同步关联协议调用及回调对应关系
    invokeForCallback(type, action, callBack, data) {
        var seqNum = this.generateCmdSeq();
        this.registerCallback(seqNum, callBack);
        this.invoke(type, action, seqNum, data)
    }

    //设置对应协议调用加载到对象上的方法
    registerCallback(seq, callBack) {
        this._pfnSet(seq, callBack)
    }

    //获取对应协议调用加载到对象上的方法
    processNativeCallback(seq, data) {
        var fn = this._pfnGet(seq);
        if (fn) {
            this._pfnRemove(seq);
            fn(data);
        }
    }

    //初始化协议
    init(Name){
        let name = Name || this.agreement;
        window[`${name}`] = this;
        window[`${this.callback}`] = function (a, b) {
            try {
                window[`${name}`].processNativeCallback(a, b)
            } catch (c) {
                tips('协议回调错误');
            }
        }
    }

    /* 具体协议 */
    getCommands(a) {
        this.invokeForCallback("function", "getCommands", a)
    }

    getPages(a) {
        this.invokeForCallback("function", "getPages", a)
    }

    installApp(a, b) {
        this.invokeForCallback("function", "installApp", b, {
            url: a
        })
    }

    submitPayOrder(a, b, c) {
        var d = b ? this.jsonToUrlEncode(b) : "";
        this.invokeForCallback("function", "submitPayOrder", c, {
            channelId: a,
            channelParams: d
        })
    }

    getClientInfo() {
        var a = {},
            b = navigator?navigator.userAgent:"",
            c = /ischool\-(\w+)\-([\d\.]+)(?:-([\d\.]+))?/,
            c1 = /shouxiner\-(\w+)\-([\d\.]+)(?:-([\d\.]+))?/,
            d = b.match(c) || b.match(c1);
        return d && (a.platform = d[1], a.version = d[3] ? d[3] : d[2]), a.clientType = b.indexOf("teacher") > 0 ? "teacher" : "parent", a
    }

    setTitleBarVisible(a, b) {
        this.invokeForCallback("function", "setTitleBarVisible", b, {
            isVisible: a
        })
    }

    setTitleBarVisibleV2(a, b) {
        this.invokeForCallback("function", "setTitleBarVisibleV2", b, {
            isVisible: a
        })
    }

    getCurrentGroupId(callBack) {
        this.invokeForCallback("function", "getCurrentGroupId", callBack)
    }

    getCurrentSchoolId(callBack) {
        this.invokeForCallback("function", "getCurrentSchoolId", callBack)
    }

    shareToWeixin(obj, callback) {
        this.invokeForCallback("function", "shareToMM", callback, obj);
    }

    getLocate(a) {
        this.invokeForCallback("function", "getLocate", a)
    };

    setTitle({title=''}){
        document.title = title;
    }

    /*
    * 拨打电话。
    * @param {String} phone 电话号。
    * */
    callphone({phone=''}){
        window.location.href = `tel:${Number(phone)}`
    }

    scanGraphicCode(backFn) {
        this.invokeForCallback("function", "scanGraphicCode", backFn)
    }

    publishTopic(a, b, c, d, e, f) {
        this.invokeForCallback("function", "publishTopic", f, {
            activityId: a,
            title: b,
            content: c,
            topicType: d,
            shouldImg: e
        })
    }

    playMedia(a, b, c, d, e) {
        this.invokeForCallback("function", "playMedia", e, {
            uri: a,
            type: b,
            name: c,
            avatar: d
        })
    }

    setWebViewSize(a, b, c) {
        this.invokeForCallback("function", "setWebViewSize", c, {
            width: a,
            height: b
        })
    }

    open(a, b) {
        this.invokeForCallback("function", "open", b, {
            uri: a
        })
    }

    close(a) {
        this.invokeForCallback("function", "close", a)
    }

    playVOB(a, b) {
        this.invokeForCallback("function", "playVOB", b, {
            url: a
        })
    }

    openLiveRoom(a, b, c, d, e, f, g, h) {
        this.invokeForCallback("page", "microcourse-room", h, {
            liveUrl: a,
            liveUrlRTMP: b,
            wisId: c,
            chatUrl: d,
            title: e,
            type: f,
            avatar: g
        })
    }

    recVideo(a, b) {
        this.invokeForCallback("function", "recVideo", b, {
            maxDuration: a
        })
    }

    selectPic(a, b) {
        this.invokeForCallback("function", "selectPic", b, {
            maxCount: a
        })
    }

    selectVideo(a) {
        this.invokeForCallback("function", "selectVideo", a)
    }

    capturePic(a) {
        this.invokeForCallback("function", "capturePic", a)
    }

    captureVideo(a, b) {
        this.invokeForCallback("function", "captureVideo", b, {
            maxDuration: a
        })
    }

    liveVideo (a, b) {
        this.invokeForCallback("page", "live-video", b, {
            url: a
        });
    }

    /* 川教通 切换tab*/
    goToTab(a,b){
        this.invokeForCallback("function", "goToTab", b, {
            index: a
        })
    }
    /* 川教通 调起app */
    launchApp(a,b,c){
        this.invokeForCallback("function", "launchApp", c, {
            package: a,
            url: b,
        })
    }

    /* 川教通 获取经纬度 */
    getLocation(a) {
        this.invokeForCallback("function", "getLocation",a)
    }

    /* 川教通 获取SUID */
    getSUID(a) {
        this.invokeForCallback("function", "getSUID",a)
    }
    
    /*对讲机 直播流播放*/
    safetyVideo(a,b){
        this.invokeForCallback("function", "safetyVideo", b, {
            url: a
        })
    }
    /*v10 直播、录播播放*/
    video_player(a,b){
        this.invokeForCallback("page", "media/video_player", b, {
            dataSource: a
        })
    }

    /*对讲机 切换tab*/
    safetyChangeTab(a,b){
        this.invokeForCallback("function", "safetyChangeTab", b, {
            choseTab: a
        })
    }

    isNewProtocol() {
        if (null == this.mIsNewProtocol) {
            var a = this.getClientInfo(),
                b = a.version;
            if (b) {
                var c = null;
                if ("ios" == a.platform) c = "4.3.1";
                else {
                    if ("android" != a.platform) return this.mIsNewProtocol = !1;
                    c = "teacher" == a.clientType ? "4.3.2767" : "4.3.2813"
                }
                this.mIsNewProtocol = b >= c
            } else this.mIsNewProtocol = !1
        }
        return this.mIsNewProtocol
    }
}

export {NativeToH5};
