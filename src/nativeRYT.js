import tips from './tips.js'
import { ryt } from 'ryt-jssdk'

class NativeToH5 {
    constructor(agreement, callback) {
        //方法id
        this._seq = 1;
        //未知有地方在用
        this.mIsNewProtocol = null;
        // 所处环境
        this.platform = '';
        //协议前缀
        this.agreement = agreement || 'ischool';
        //原生监听方法
        this.callback = callback || 'onShouxinerSchemeCallback';
    }

    /* 内置方法 */

    //增加协议对象
    _pfnSet(fn, callback) {
        this[fn] = callback
    }

    //获取协议对象
    _pfnGet(fn) {
        return this[fn]
    }

    //删除协议对象
    _pfnRemove(fn) {
        delete this[fn]
    }

    //调用客户端协议
    invoke(type, action, seqNum, data) {
        //var uri = "ischool://" + type + "/" + action + "?cmdSeq=" + seqNum;
        var uri = `${this.agreement}://${type}/${action}?cmdSeq=${seqNum}`;
        if (data) {
            uri += "&" + this.jsonToUrlEncode(data);
        }
        window.location = uri
    }

    //传参格式化
    jsonToUrlEncode(data) {
        var str = "";
        if (data) {
            for (var item in data) {
                if (str.length > 0) {
                    str += "&"
                }
                str += (item + '=' + encodeURIComponent(data[item]));
            }
        }
        return str
    }

    //获取唯一id
    generateCmdSeq() {
        return this._seq++
    }

    //同步关联协议调用及回调对应关系
    invokeForCallback(type, action, callBack, data) {
        var seqNum = this.generateCmdSeq();
        this.registerCallback(seqNum, callBack);
        this.invoke(type, action, seqNum, data)
    }

    //设置对应协议调用加载到对象上的方法
    registerCallback(seq, callBack) {
        this._pfnSet(seq, callBack)
    }

    //获取对应协议调用加载到对象上的方法
    processNativeCallback(seq, data) {
        var fn = this._pfnGet(seq);
        if (fn) {
            this._pfnRemove(seq);
            fn(data);
        }
    }

    //初始化协议
    init(Name) {
        let name = Name || this.agreement;
        window[`${name}`] = this;
    }

    getClientInfo(){
        return {
            clientType:'teacher',
            platform:'ryt',
            version:'10.0.0',
        }
    }

    /* 具体协议 */

    /*
    * 设置title
     * @param {string} title 标题
    * */
    setTitle({title=''}){
        ryt.setTitle(title);
    }

    /*
    * 调整屏幕亮度
    * @param {init} brightness 0.0~1.0
    * @param {function} backFn 回调
    * */
    setBrightness({brightness=0}, backFn) {
        this.invokeForCallback("function", "setBrightness", backFn, {
            brightness: parseFloat(brightness).toFixed(1)
        })
    }

    /*
    * 附件预览
    * @param{string} fileUrl 预览附件的地址
    * @param {string} fileType 附件类型
    * @param {function} backFn 回调
    * */
    filePreview(fileUrl, fileType, backFn) {
        let fileTypeArrOffice = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf'];
        let fileTypeArrImage = ['jpg', 'png', 'jpeg', 'gif', 'mp4'];
        if (fileTypeArrOffice.includes(fileType)) { //office预览
            backFn && backFn({
                resultCode: 4,
                resultMsg: "请在pc端查看",
            });
            tips("请在pc端查看")
        } else if (fileTypeArrImage.includes(fileType)) { //图片、视频预览
            $('.preview-wrap').remove();
            var preview = '';
            if (fileType === 'mp4') {
                preview = $('<div class="preview-wrap"><video class="preview" src="' + fileUrl + '"/></div>');
            } else {
                preview = $('<div class="preview-wrap"><img alt="加载失败" class="preview" src="' + fileUrl + '"/></div>');
            }
            $('body').append(preview);
            $('body').on('click', '.preview-wrap', () => {
                $('.preview-wrap').remove();
            })
            $('body').on('click', '.preview', (e) => {
                return false
            })
            preview.css({
                'position': 'fixed',
                'top': 0,
                'left': 0,
                'width': '100%',
                'height': '100%',
                'z-index': '1000000',
                'background': '#333333',
                'display': 'none'
            });
            preview.find('.preview').css({
                'position': 'absolute',
                'top': '0',
                'left': '0',
                'width': '100%',
                'height': '100%',
                'object-fit': 'contain',
            });
        } else { //其他类型
            backFn && backFn({
                resultCode: 4,
                resultMsg: "请在pc端查看",
            });
            tips("请在pc端查看")
        }
    }

    /*
    * 设置是否隐藏标题栏，该协议仅对标题栏生效，不影响顶部的系统状态栏，无沉浸式效果。
    * @param {bool} isVisible true 显示标题栏；false 隐藏标题栏。
    * @param {function} backFn 回调
    * */
    setTitleBarVisible({isVisible=true}, backFn) {
        backFn && backFn({
            resultCode: 4,
            resultMsg: "不支持该协议",
        });
    }

    /*
    * 设置是否隐藏标题栏，该协议对标题栏及系统状态栏同时生效，带沉浸式效果。
    * @param {bool} isVisible true 显示标题栏；false 隐藏标题栏。
    * @param {function} backFn 回调
    * */
    setTitleBarVisibleV2({isVisible=true}, backFn) {
        backFn && backFn({
            resultCode: 4,
            resultMsg: "不支持该协议",
        });
    }

    /*
    * 打开新Web页或本地页。
    * @param {string} uri 页面地址。当为 http/https 协议时，打开Web页。当为 ischool://page 协议时，打开原生页。
    * @param {bool} ignoreHistory 默认true 忽略页内导航栈；false 不忽略页内导航栈。
    * @param {function} backFn 回调
    * */
    open({uri='', ignoreHistory=true,title="加载中"},backFn) {
        ryt.openNewWebView({ url: uri ,title:title})
        backFn && backFn({
            resultCode: 0,
            resultMsg: "成功",
        });
    }

    /*
    * 设置Web页是否忽略页内导航栈。
    * @param {bool} ignore 是否忽略页内导航栈。true 忽略页内导航栈；false 不忽略页内导航栈。
    * @param {function} backFn 回调
    * */
    ignoreHistory({ignore=true}, backFn) {
        this.invokeForCallback("function", "ignoreHistory", backFn, {
            ignore: ignore
        })
    }

    /*
    * 关闭当前Web页。
    * @param {function} backFn 回调
    * */
    close(backFn) {
        this.invokeForCallback("function", "close", backFn)
    }

    /*
    * 获取当前选择的班级ID。
    * @param {function} backFn 回调
    * */
    getCurrentGroupId(backFn) {
        this.invokeForCallback("function", "getCurrentGroupId", backFn)
    }

    /*
    * 获取当前选择班级的所在学校ID。
    * @param {function} backFn 回调
    * */
    getCurrentSchoolId(backFn) {
        this.invokeForCallback("function", "getCurrentSchoolId", backFn)
    }

    /*
    * 拨打点话。
    * */
    callphone({phone=''}){
        ryt.callphone(phone)
    }

    /*
    * 扫码并获取结果。
    * @param {function} backFn 回调
    * */
    scanGraphicCode(backFn) {
        ryt.scan({
            "goBackDirectly": true,
            "ruleId":"ryt",
        }, (res) => {
            res.then((data)=>{
                backFn({
                    resultCode:0, //0为成功
                    resultMsg:"成功", //错误信息
                    content:data   // 扫码内容
                })
            }).catch((e)=>{
                backFn({
                    resultCode:-1, //0为成功
                    resultMsg:"失败", //错误信息
                    content:e   // 扫码内容
                })
            })
        });
    }

    /*
    * 录制并上传视频。该协议仅返回上传成功后的附件地址。
    * @param {number} maxDuration 最大录制时长，单位秒。
    * @param {function} backFn 回调
    * */
    recVideo({maxDuration=0}, backFn) {
        this.invokeForCallback("function", "recVideo", backFn, {
            maxDuration: maxDuration
        })
    }

    /*
    * 从相册选择并上传照片。该协议会返回上传成功后服务器返回的原始内容。
    * @param {number} maxCount 最大录制时长，单位秒。
    *  @param {string} uploadUrl 附件服务器地址
    * @param {function} backFn 回调
    * */
    selectPic({maxCount=0,uploadUrl=''}, backFn) {
        let param = Object.assign({},
            maxCount ? {maxCount}:null,
            uploadUrl ? {uploadUrl}:null,
        );
        this.invokeForCallback("function", "selectPic", backFn, param)
    }

    /*
    * 从相册选择并上传视频。该协议会返回上传成功后服务器返回的原始内容。目前仅可选择单个时长不超过5分钟的视频。
    * @param {string} uploadUrl 附件服务器地址
    * @param {function} backFn 回调
    * */
    selectVideo({uploadUrl=''},backFn) {
        let param = Object.assign({},
            uploadUrl ? {uploadUrl}:null,
        );
        this.invokeForCallback("function", "selectVideo", backFn,param)
    }

    /*
    * 拍摄并上传照片。该协议会返回上传成功后服务器返回的原始内容。
    * @param {string} uploadUrl 附件服务器地址
    * @param {function} backFn 回调
    * */
    capturePic({uploadUrl=''},backFn) {
        let param = Object.assign({},
            uploadUrl ? {uploadUrl}:null,
        );
        this.invokeForCallback("function", "capturePic", backFn,param)
    }

    /*
    * 录制并上传视频。该协议会返回上传成功后服务器返回的原始内容。
    * @param {number} maxDuration 最大录制时长，单位秒。
    * @param {string} uploadUrl 附件服务器地址
    * @param {function} backFn 回调
    * */
    captureVideo({maxDuration=0,uploadUrl=''}, backFn) {
        let param = Object.assign({},
            maxDuration ? {maxDuration}:null,
            uploadUrl ? {uploadUrl}:null,
        );
        this.invokeForCallback("function", "captureVideo", backFn, param)
    }

    /*
    * 获取定位信息。(V10未实现)
    * @param {function} backFn 回调
    * */
    getLocation(backFn) {
        this.invokeForCallback("function", "getLocation", backFn)
    }

    /*
    * 获取应用当前状态快照（用户信息、孩子信息、机构信息、客户端类型）。
    * @param {function} backFn 回调
    * */
    getAppSnapshot(backFn) {
        this.invokeForCallback("function", "getAppSnapshot", backFn)
    }

    /*
    * 调起指定应用。
    * @param {String} androidPackageName Android端应用包名。
    * @param {String} iOSUrlScheme iOS端应用声明的用于打开应用的Url Scheme。
    * @param {String} appStoreLink iOS端应用在App Store上的地址。
    * @param {function} backFn 回调
    * */
    launchApp({androidPackageName='', iOSUrlScheme='', appStoreLink=''}, backFn) {
        let param = Object.assign({},
            androidPackageName ? {androidPackageName}:null,
            iOSUrlScheme ? {iOSUrlScheme}:null,
            appStoreLink ? {appStoreLink}:null,
        );
        this.invokeForCallback("function", "launchApp", backFn, param)
    }

    /*
    * 分享至微信。
    * @param {number} scene 待分享的场景，有效取值在1-3之间。1 代表朋友圈；2 代表会话；3 代表收藏。
    * @param {number} shareModel 待分享的类型，有效取值在1-2之间。1 代表分享文本；2 代表分享网页。
    * @param {String} text 待分享文本。
    * @param {String} webPage 待分享网页地址。
    * @param {String} title 待分享网页标题。
    * @param {String} description 待分享网页描述。
    * @param {String} thumbnail 待分享网页封面。
    * @param {function} backFn 回调
    * */
    shareToWeChat({scene=0, shareModel=0, text='', webPage='', title='', description='', thumbnail=''}, backFn) {
        let param = Object.assign({},
            scene ? {scene}:null,
            shareModel ? {shareModel}:null,
            text ? {text}:null,
            webPage ? {webPage}:null,
            title ? {title}:null,
            description ? {description}:null,
            thumbnail ? {thumbnail}:null,
        );
        this.invokeForCallback("function", "shareToWeChat", backFn, param)
    }

    /*
    * 调起指定微信小程序。
    * @param {string} id 待调起的小程序ID。
    * @param {function} backFn 回调
    * */
    launchMiniProgram({id=''}, backFn) {
        this.invokeForCallback("function", "launchMiniProgram", backFn, {
            id: id
        })
    }

    /*
    * 获取OAuth2认证的 Access Token。
    * @param {function} backFn 回调
    * */
    getOAuth2AccessToken(backFn) {
        this.invokeForCallback("function", "getOAuth2AccessToken", backFn)
    }

    /*
    * 调起支付
    * @param {number} channelId 支付渠道，有效取值在1-2之间。1 代表支付宝；2 代表微信。
    * @param {object} channelParams 对应支付渠道的支付参数。
    * @param {function} backFn 回调
    * */
    submitPayOrder({channelId=0, channelParams=''}, backFn) {
        let param = Object.assign({},
            channelId ? {channelId}:null,
            channelParams ? this.jsonToUrlEncode(channelParams):null,
        );
        this.invokeForCallback("function", "submitPayOrder", backFn, param)
    }

    /*
    * 选取媒体文件。该协议不会对选取的文件进行上传，仅返回Web端可访问的本地文件地址。
    * @param {Array} mediaType 可选的媒体类型。支持 ['image', 'video'] 。'image' 代表图片；'video' 代表视频。
    * @param {Array} sourceType 媒体选取的数据源。支持 ['album', 'camera'] 。'album' 代表从相册中选择；'camera' 代表使用摄像头选
    * @param {Number} count 最多可选取的文件个数。
    * @param {Number} maxDuration 视频的最大录制时长，单位秒。不限制从相册选取的视频时长。
    * @param {Array} sizeType 是否对媒体文件进行压缩。支持 ['original', 'compressed'] 。'original' 代表使用原始文件；'compressed' 代表对文件进行压缩处理。仅支持单个选项，当数组传入多个值时，以第一元素为准，剩余元素会被忽略。
    * @param {string} camera 使用的摄像头类型。'back' 代表后置摄像头；'front' 代表前置摄像头。
    * @param {function} backFn 回调
    * */
    chooseMedia({mediaType=[], sourceType=[], count=0, maxDuration=0, sizeType=[], camera='back'}, backFn) {
        let param = Object.assign({},
            mediaType.length ? {mediaType:JSON.stringify(mediaType)}:null,
            sourceType.length ? {sourceType:JSON.stringify(sourceType)}:null,
            count ? {count}:null,
            maxDuration ? {maxDuration}:null,
            sizeType.length ? {sizeType:JSON.stringify(sizeType)}:null,
            camera ? {camera}:null,
        );
        this.invokeForCallback("function", "chooseMedia", backFn, param)
    }

    /*
    * 上传媒体文件。该协议仅支持通过 chooseMedia 协议选取的媒体文件。
    * @param {String} url 附件上传地址。若不传则使用默认的附件服务器地址。
    * @param {String} filePath 单个媒体文件地址，地址需要确保是通过调用 chooseMedia 协议获取的。
    * @param {Array} filePaths 多个媒体文件地址列表，地址需要确保是通过调用 chooseMedia 协议获取的。
    * @param {String} fileName 上传单个媒体文件时的文件名。
    * @param {Array} fileNames 上传多个媒体文件时的文件名列表。
    * @param {Object} header 上传文件时的额外请求头。
    * @param {Object} formData 上传文件时的额外表单数据。
    * @param {function} backFn 回调
    * */
    uploadMedia({url='', filePath='', filePaths=[], fileName='', fileNames=[], header='',formData=''}, backFn) {
        let param = Object.assign({},
            url ? {url}:null,
            filePath ? {filePath}:null,
            filePaths.length ? {filePaths:JSON.stringify(filePaths)}:null,
            fileName ? {fileName}:null,
            fileNames.length ? {fileNames:JSON.stringify(fileNames)}:null,
            header ? {header:JSON.stringify(header)}:null,
            formData ? {formData:JSON.stringify(formData)}:null,
        );
        this.invokeForCallback("function", "uploadMedia", backFn, param)
    }
}

export {NativeToH5};
