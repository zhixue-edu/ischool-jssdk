function Ischool() {
    var a = 1,
        b = {
            set: function (a, b) {
                this[a] = b
            },
            get: function (a) {
                return this[a]
            },
            contains: function (a) {
                return null == this.get(a) ? !1 : !0
            },
            remove: function (a) {
                delete this[a]
            }
        };
    this.getCommands = function (a) {
        this.invokeForCallback("function", "getCommands", a)
    }, this.getPages = function (a) {
        this.invokeForCallback("function", "getPages", a)
    }, this.installApp = function (a, b) {
        this.invokeForCallback("function", "installApp", b, {
            url: a
        })
    }, this.submitPayOrder = function (a, b, c) {
        var d = b ? this.jsonToUrlEncode(b) : "";
        this.invokeForCallback("function", "submitPayOrder", c, {
            channelId: a,
            channelParams: d
        })
    }, this.processNativeCallback = function (a, c) {
        var d = b.get(a);
        d && (b.remove(a), d(c))
    }, this.generateCmdSeq = function () {
        return a++
    }, this.invoke = function (a, b, c, d) {
        var e = "ischool://" + a + "/" + b + "?cmdSeq=" + c;
        d && (e += "&" + this.jsonToUrlEncode(d)), window.location = e
    }, this.jsonToUrlEncode = function (a) {
        var b = "";
        if (a)
            for (var c in a) b.length > 0 && (b += "&"), b += c + "=" + encodeURIComponent(a[c]);
        return b
    }, this.registerCallback = function (a, c) {
        b.set(a, c)
    }, this.invokeForCallback = function (a, b, c, d) {
        var e = this.generateCmdSeq();
        this.registerCallback(e, c), this.invoke(a, b, e, d)
    }, this.getClientInfo = function () {
        var a = {},
            b = navigator?navigator.userAgent:"",
            c = /ischool\-(\w+)\-([\d\.]+)(?:-([\d\.]+))?/,
            c1 = /shouxiner\-(\w+)\-([\d\.]+)(?:-([\d\.]+))?/,
            d = b.match(c) || b.match(c1);
        return d && (a.platform = d[1], a.version = d[3] ? d[3] : d[2]), a.clientType = b.indexOf("teacher") > 0 ? "teacher" : "parent", a
    }, this.setTitleBarVisible = function (a, b) {
        this.invokeForCallback("function", "setTitleBarVisible", b, {
            isVisible: a
        })
    }, this.setTitleBarVisibleV2 = function (a, b) {
        this.invokeForCallback("function", "setTitleBarVisibleV2", b, {
            isVisible: a
        })
    }, this.getCurrentGroupId = function (callBack) {
        this.invokeForCallback("function", "getCurrentGroupId", callBack)
    }, this.getCurrentSchoolId = function (callBack) {
        this.invokeForCallback("function", "getCurrentSchoolId", callBack)
    },
        this.shareToWeixin = function (obj, callback) {
            this.invokeForCallback("function", "shareToMM", callback, obj);
        }, this.getLocate = function (a) {
        this.invokeForCallback("function", "getLocate", a)
    }, this.publishTopic = function (a, b, c, d, e, f) {
        this.invokeForCallback("function", "publishTopic", f, {
            activityId: a,
            title: b,
            content: c,
            topicType: d,
            shouldImg: e
        })
    }, this.playMedia = function (a, b, c, d, e) {
        this.invokeForCallback("function", "playMedia", e, {
            uri: a,
            type: b,
            name: c,
            avatar: d
        })
    }, this.setWebViewSize = function (a, b, c) {
        this.invokeForCallback("function", "setWebViewSize", c, {
            width: a,
            height: b
        })
    }, this.open = function (a, b) {
        this.invokeForCallback("function", "open", b, {
            uri: a
        })
    }, this.close = function (a) {
        this.invokeForCallback("function", "close", a)
    }, this.playVOB = function (a, b) {
        this.invokeForCallback("function", "playVOB", b, {
            url: a
        })
    }, this.openLiveRoom = function (a, b, c, d, e, f, g, h) {
        this.invokeForCallback("page", "microcourse-room", h, {
            liveUrl: a,
            liveUrlRTMP: b,
            wisId: c,
            chatUrl: d,
            title: e,
            type: f,
            avatar: g
        })
    }, this.recVideo = function (a, b) {
        this.invokeForCallback("function", "recVideo", b, {
            maxDuration: a
        })
    }, this.selectPic = function (a, b) {
        this.invokeForCallback("function", "selectPic", b, {
            maxCount: a
        })
    }, this.selectVideo = function (a) {
        this.invokeForCallback("function", "selectVideo", a)
    }, this.capturePic = function (a) {
        this.invokeForCallback("function", "capturePic", a)
    }, this.captureVideo = function (a, b) {
        this.invokeForCallback("function", "captureVideo", b, {
            maxDuration: a
        })
    }, this.mIsNewProtocol = null, this.isNewProtocol = function () {
        if (null == this.mIsNewProtocol) {
            var a = this.getClientInfo(),
                b = a.version;
            if (b) {
                var c = null;
                if ("ios" == a.platform) c = "4.3.1";
                else {
                    if ("android" != a.platform) return this.mIsNewProtocol = !1;
                    c = "teacher" == a.clientType ? "4.3.2767" : "4.3.2813"
                }
                this.mIsNewProtocol = b >= c
            } else this.mIsNewProtocol = !1
        }
        return this.mIsNewProtocol
    }
}

let ischool = new Ischool();

let onShouxinerSchemeCallback = function (a, b) {
    try {
        window.ischool.processNativeCallback(a, b)
    } catch (c) {
        alert('协议回调错误');
    }
};

export {ischool, onShouxinerSchemeCallback};
