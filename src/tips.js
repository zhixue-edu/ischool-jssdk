import '../lib/jquery.min.js'
function tips(msg, t, fn) {
  $('.tips-wrap').remove();
  var tips = $('<div class="tips-wrap"><span class="tips">' + msg + '</span></div>');
  $('body').append(tips);
  var t = t || 2000;
  tips.css({
    'position': 'fixed',
    'font-size': 'px2em(30)',
    'top': 0,
    'left': 0,
    'width': '100%',
    'height': '100%',
    'z-index': '1000000',
    'display': 'none'
  });
  tips.find('.tips').css({
    'background': 'rgba(0,0,0,0.7)',
    'color': '#ffffff',
    'position': 'absolute',
    'top': '50%',
    'left': '50%',
    'transform': 'translate(-50%,-50%)',
    'padding': '15px',
    'border-radius': '10px',
    'text-align': 'center'
  });
  $('.tips-wrap').fadeIn();
  setTimeout(function () {
    $('.tips-wrap').fadeOut(function () {
      $('.tips-wrap').remove();
      fn && fn()
    });
  }, t)
}

export default tips;
