class NativeToH5 {
    constructor() {
    }

    /* 具体协议 */

    /*
     * 拨打电话。
     * @param {String} phone 电话号。
     * */
    callphone({phone = ''}) {
        wx.makePhoneCall({
            phoneNumber: phone + ''
        })
    }

    /*
    * 调整屏幕亮度
    * @param {number double} brightness 0.0~1.0
    * @param {function} backFn 回调
    * */
    setBrightness(value, backFn) {
        wx.setScreenBrightness({
            value: parseFloat(value).toFixed(1),
            success: () => {
                backFn && backFn({
                    resultCode: 0,
                    resultMsg: "设置成功",
                });
                console.log('设置成功')
            },
            fail: () => {
                backFn && backFn({
                    resultCode: 1,
                    resultMsg: "设置失败",
                });
                console.log('设置失败')
            }
        })
    }

    /*
    * 附件预览
    * @param {string} fileUrl 预览附件的地址
    * @param {string} fileType 附件类型
    * @param {function} backFn 回调
    * */
    filePreview(fileName, fileUrl, fileType, backFn) {
        let fileTypeArrOffice = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf'];
        let fileTypeArrImage = ['jpg', 'png', 'jpeg', 'gif', 'mp4'];
        if (fileTypeArrOffice.includes(fileType)) { //office预览
            wx.showLoading({
                title: '文件加载中',
                icon: 'none'
            });
            wx.downloadFile({
                url: fileUrl, //资源路径
                filePath: wx.env.USER_DATA_PATH + '/' + fileName,
                success(res) {
                    wx.openDocument({
                        showMenu: true,
                        filePath: res.filePath,//资源临时路径
                        fileType: fileType,//资源类型
                        success: function (res) {
                            backFn && backFn({
                                resultCode: 0,
                                resultMsg: "打开文件成功",
                            });
                            console.log('打开文件成功')
                        },
                        fail(e) {
                            backFn && backFn({
                                resultCode: 1,
                                resultMsg: "打开文件失败",
                            });
                            console.log('打开文件失败', e);
                            wx.showToast({
                                title: '打开文件失败',
                                icon: 'none'
                            });
                        }
                    });
                    wx.hideLoading();
                },
                fail(e) {
                    backFn && backFn({
                        resultCode: 2,
                        resultMsg: "下载文件失败",
                    });
                    console.log('下载文件失败', e)
                    wx.hideLoading();
                    wx.showToast({
                        title: '下载文件失败',
                        icon: 'none'
                    });
                }
            })
        } else if (fileTypeArrImage.includes(fileType)) { //图片、视频预览
            wx.previewMedia({
                sources: [{url: fileUrl, type: fileType === 'mp4' ? 'video' : 'image'}],
                success: (e) => {
                    backFn && backFn({
                        resultCode: 0,
                        resultMsg: "预览成功",
                    });
                    console.log('预览成功', e)
                },
                fail: (e) => {
                    backFn && backFn({
                        resultCode: 3,
                        resultMsg: "预览失败",
                    });
                    console.log('预览失败', e)
                    wx.showToast({
                        title: '预览失败',
                        icon: 'none'
                    });
                }
            })
        } else { //其他类型
            backFn && backFn({
                resultCode: 4,
                resultMsg: "请在pc端查看",
            });
            console.log('请在pc端查看')
            wx.showToast({
                title: '请在pc端查看',
                icon: 'none'
            });
        }

    }

    /*
    * office、图片、视频分享
    * @param {string} fileUrl 分享内容的地址
    * @param {string} fileType 分享内容类型
    * @param {function} backFn 回调
    * */
    fileShare(fileName, fileUrl, fileType, backFn) {
        let fileTypeArrOffice = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'pdf'];
        let fileTypeArrImage = ['jpg', 'png', 'jpeg', 'gif'];
        let fileTypeArrVideo = ['mp4'];
        wx.showLoading({
            title: '文件加载中',
            icon: 'none'
        });
        wx.downloadFile({
            url: fileUrl, //资源路径
            success(res) {
                if (fileTypeArrOffice.includes(fileType)) { //office分享
                    wx.shareFileMessage({
                        fileName: fileName || '',//资源临时路径
                        filePath: res.tempFilePath,//资源临时路径
                        success: function (res) {
                            backFn && backFn({
                                resultCode: 0,
                                resultMsg: "分享文件成功",
                            });
                            console.log('打开文件成功')
                        },
                        fail(e) {
                            backFn && backFn({
                                resultCode: 1,
                                resultMsg: "分享文件失败",
                            });
                            console.log('分享文件失败', e);
                            wx.showToast({
                                title: '分享文件失败',
                                icon: 'none'
                            });
                        }
                    });
                } else if (fileTypeArrImage.includes(fileType)) { //图片分享
                    wx.showShareImageMenu({
                        path: res.tempFilePath,//资源临时路径
                        success: (e) => {
                            backFn && backFn({
                                resultCode: 0,
                                resultMsg: "分享成功",
                            });
                            console.log('分享成功', e)
                        },
                        fail: (e) => {
                            backFn && backFn({
                                resultCode: 3,
                                resultMsg: "分享失败",
                            });
                            console.log('分享失败', e)
                            wx.showToast({
                                title: '分享失败',
                                icon: 'none'
                            });
                        }
                    })
                } else if (fileTypeArrVideo.includes(fileType)) { //视频分享
                    wx.shareVideoMessage({
                        videoPath: res.tempFilePath,//资源临时路径
                        success: (e) => {
                            backFn && backFn({
                                resultCode: 0,
                                resultMsg: "分享成功",
                            });
                            console.log('分享成功', e)
                        },
                        fail: (e) => {
                            backFn && backFn({
                                resultCode: 3,
                                resultMsg: "分享失败",
                            });
                            console.log('分享失败', e)
                            wx.showToast({
                                title: '分享失败',
                                icon: 'none'
                            });
                        }
                    })
                } else { //其他类型
                    backFn && backFn({
                        resultCode: 4,
                        resultMsg: "不支持该类型文件的分享",
                    });
                    console.log('不支持该类型文件的分享')
                    wx.showToast({
                        title: '不支持该类型文件的分享',
                        icon: 'none'
                    });
                }
                wx.hideLoading();
            },
            fail(e) {
                backFn && backFn({
                    resultCode: 2,
                    resultMsg: "下载文件失败",
                });
                console.log('下载文件失败', e)
                wx.hideLoading();
                wx.showToast({
                    title: '下载文件失败',
                    icon: 'none'
                });
            }
        })
    }

    /*
    * 获取应用当前状态快照（用户信息、孩子信息、机构信息、客户端类型）。
    * @param {function} backFn 回调
    * */
    getAppSnapshot(backFn) {
        let gidObj = JSON.parse(wx.getStorageSync('gidObj'));
        backFn({
            resultCode: 0,
            resultMsg: "",
            snapshot: {
                user: {					// 用户信息
                    id: wx.getStorageSync('uid'),				// 用户ID
                    name: "",			// 用户名
                    avatar: gidObj.avatar			// 用户头像地址
                },
                kid: {					// 当前选中的孩子信息（仅家长端有效）
                    id: gidObj.childrenUid,				// 孩子ID
                    name: gidObj.childrenName,			// 孩子名
                    avatar: gidObj.childrenAvatar,			// 孩子头像地址
                },
                organization: {			// 当前选中机构信息
                    id: gidObj.groupid,				// 机构ID
                    name: gidObj.cname,			// 机构名
                    superiorId: gidObj.upstreamid,		// 上级机构ID
                    superiorName: gidObj.school,	// 上级机构名
                    groupType: gidObj.grouptype,		// 机构类型
                    memberType: gidObj.membertype	// 在该机构下的身份类型
                },
                flavor: wx.getExtConfigSync().role			// 渠道信息：家长端或教师端
            }
        })
    }

    getCommands(a) {
        this.invokeForCallback("function", "getCommands", a)
    }

    getPages(a) {
        this.invokeForCallback("function", "getPages", a)
    }

    installApp(a, b) {
        this.invokeForCallback("function", "installApp", b, {
            url: a
        })
    }

    submitPayOrder(a, b, c) {
        var d = b ? this.jsonToUrlEncode(b) : "";
        this.invokeForCallback("function", "submitPayOrder", c, {
            channelId: a,
            channelParams: d
        })
    }

    setTitleBarVisible(a, b) {
        this.invokeForCallback("function", "setTitleBarVisible", b, {
            isVisible: a
        })
    }

    setTitleBarVisibleV2(a, b) {
        this.invokeForCallback("function", "setTitleBarVisibleV2", b, {
            isVisible: a
        })
    }

    getCurrentGroupId(callBack) {
        this.invokeForCallback("function", "getCurrentGroupId", callBack)
    }

    getCurrentSchoolId(callBack) {
        this.invokeForCallback("function", "getCurrentSchoolId", callBack)
    }

    shareToWeixin(obj, callback) {
        this.invokeForCallback("function", "shareToMM", callback, obj);
    }

    getLocate(a) {
        this.invokeForCallback("function", "getLocate", a)
    };

    publishTopic(a, b, c, d, e, f) {
        this.invokeForCallback("function", "publishTopic", f, {
            activityId: a,
            title: b,
            content: c,
            topicType: d,
            shouldImg: e
        })
    }

    playMedia(a, b, c, d, e) {
        this.invokeForCallback("function", "playMedia", e, {
            uri: a,
            type: b,
            name: c,
            avatar: d
        })
    }

    setWebViewSize(a, b, c) {
        this.invokeForCallback("function", "setWebViewSize", c, {
            width: a,
            height: b
        })
    }

    open(a, b) {
        this.invokeForCallback("function", "open", b, {
            uri: a
        })
    }

    close(a) {
        this.invokeForCallback("function", "close", a)
    }

    playVOB(a, b) {
        this.invokeForCallback("function", "playVOB", b, {
            url: a
        })
    }

    openLiveRoom(a, b, c, d, e, f, g, h) {
        this.invokeForCallback("page", "microcourse-room", h, {
            liveUrl: a,
            liveUrlRTMP: b,
            wisId: c,
            chatUrl: d,
            title: e,
            type: f,
            avatar: g
        })
    }

    recVideo(a, b) {
        this.invokeForCallback("function", "recVideo", b, {
            maxDuration: a
        })
    }

    selectPic(a, b) {
        this.invokeForCallback("function", "selectPic", b, {
            maxCount: a
        })
    }

    selectVideo(a) {
        this.invokeForCallback("function", "selectVideo", a)
    }

    capturePic(a) {
        this.invokeForCallback("function", "capturePic", a)
    }

    captureVideo(a, b) {
        this.invokeForCallback("function", "captureVideo", b, {
            maxDuration: a
        })
    }

    liveVideo(a, b) {
        this.invokeForCallback("page", "live-video", b, {
            url: a
        });
    }

    /* 川教通 切换tab*/
    goToTab(a, b) {
        this.invokeForCallback("function", "goToTab", b, {
            index: a
        })
    }

    /* 川教通 调起app */
    launchApp(a, b, c) {
        this.invokeForCallback("function", "launchApp", c, {
            package: a,
            url: b,
        })
    }

    /* 川教通 获取经纬度 */
    getLocation(a) {
        this.invokeForCallback("function", "getLocation", a)
    }

    /* 川教通 获取SUID */
    getSUID(a) {
        this.invokeForCallback("function", "getSUID", a)
    }

    /*对讲机 直播流播放*/
    safetyVideo(a, b) {
        this.invokeForCallback("function", "safetyVideo", b, {
            url: a
        })
    }

    /*对讲机 切换tab*/
    safetyChangeTab(a, b) {
        this.invokeForCallback("function", "safetyChangeTab", b, {
            choseTab: a
        })
    }
}

export {NativeToH5};
