### 协议说明

#### 判断app环境及版本：`yourSelfName.name/yourSelfName.version`
- 说明：
```
name,version有值即为在相应app环境中
```
- 参数：无
- 例子：
```js
let name = yourSelfName.name;
let version = yourSelfName.version;
```
- 返回：无
___
#### 设置标题栏名称：`yourSelfName.setTitle`

- 参数：

| 属性 | 类型     | 默认值 | 必填  | 说明       |
|------|--------|--------|:----|:---------|
| title  | string |        | 是   | 显示的标题栏名称 |

- 例子：
```js
yourSelfName.setTitle({
    title:'测试title',
})
```
- 返回：
```js
无
```
___
#### 设置是否隐藏标题栏，该协议仅对标题栏生效，不影响顶部的系统状态栏，无沉浸式效果。：`yourSelfName.setTitleBarVisible`

- 参数：

| 属性 | 类型   | 默认值 | 必填  | 说明                                                          |
|------|--------|--------|:----|:------------------------------------------------------------|
| isVisible  | bool |        | 是   | true 显示标题栏；false 隐藏标题栏。                                          |

- 例子：
```js
yourSelfName.setTitleBarVisible({
    isVisible:false,
},(obj)=>{
    if(obj.resultCode==0){
        console.log('操作成功')
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
}
```
___
#### 设置是否隐藏标题栏，该协议对标题栏及系统状态栏同时生效，带沉浸式效果。：`yourSelfName.setTitleBarVisibleV2`

- 参数：无
- 例子：
```js
yourSelfName.setTitleBarVisibleV2({
    isVisible:false,
},(obj)=>{
    if(obj.resultCode==0){
        console.log('操作成功')
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
}
```
___
#### 打开新Web页或本地页：`yourSelfName.open`

- 参数：

| 属性 | 类型   | 默认值   | 必填  | 说明                                                          |
|------|--------|-------|:----|:------------------------------------------------------------|
| uri  | String |       | 是   | 页面地址。当为 http/https 协议时，打开Web页。当为 ischool://page 协议时，打开原生页。                                     |
| ignoreHistory  | bool | false | 否   | true 忽略页内导航栈；false 不忽略页内导航栈。                                     |
- 例子：
```js
yourSelfName.open({
    uri:'https://www.baidu.com',
    ignoreHistory:false,
}, (obj)=>{
        if(obj.resultCode==0){
            console.log('操作成功')
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
}
```
___
#### 关闭当前Web页：`yourSelfName.close`

- 参数：无
- 例子：
```js
yourSelfName.close( (obj)=> {
    if(obj.resultCode==0){
        console.log('操作成功')
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
resultCode:0, //0为成功
resultMsg:"", //错误信息
}
```
___
#### 获取当前选择的班级ID。：`yourSelfName.getCurrentGroupId`

- 参数：无
- 例子：
```js
yourSelfName.getCurrentGroupId((obj)=>{
    (obj)=> {
        if (obj.resultCode == 0) {
            let groupId = obj.groupId
        }
    }
})
```
- 返回：
```js
{
    resultCode:0,
    resultMsg:"",
    groupId:""		// 当前选择的班级ID
}

```
___
#### 获取当前选择班级的所在学校ID。：`yourSelfName.getCurrentSchoolId`
- 参数：无
- 例子：
```js
yourSelfName.getCurrentSchoolId((obj)=>{
    (obj)=> {
        if (obj.resultCode == 0) {
            let schoolId = obj.schoolId
        }
    }
})
```
- 返回：
```js
{
    resultCode:0,
    resultMsg:"",
    schoolId:""		// 当前选择班级的所在学校ID
}

```
___
#### 扫码并获取结果。：`yourSelfName.scanGraphicCode`

- 参数：无
- 例子：
```js
yourSelfName.scanGraphicCode(
    (obj)=>{
        if(obj.resultCode==0){
            let content = obj.content
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    content:'XXXXXXX'   // 扫码内容
}
```
___
#### 获取OAuth2认证的 Access Token：`yourSelfName.getOAuth2AccessToken`

- 参数：无
- 例子：
```js
yourSelfName.getOAuth2AccessToken(
    (obj)=>{
        if(obj.resultCode==0){
            let token = obj.token
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    token:'XXXXXXX'   // OAuth2认证后的 Access Token
}
```
___
#### 录制并上传视频。该协议仅返回上传成功后的附件地址。：`yourSelfName.recVideo`
- 参数：

| 属性 | 类型   | 默认值  | 必填  | 说明                                                          |
|------|--------|------|:----|:------------------------------------------------------------|
| maxDuration  | int |      | 是   | 最大录制时长，单位秒。                                     |
- 例子：
```js
yourSelfName.recVideo({
    maxDuration:15
    }, (obj)=>{
        if(obj.resultCode==0){
            let url = obj.url
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    url:'XXXXX'   // 附件服务器地址
}
```
___
#### 从相册选择并上传照片。该协议会返回上传成功后服务器返回的原始内容。：`yourSelfName.selectPic`
- 参数：

| 属性 | 类型   | 默认值  | 必填  | 说明                                                          |
|------|--------|------|:----|:------------------------------------------------------------|
| maxCount  | int |      | 是   | 最多可选取的图片个数。                                     |
| uploadUrl  | String | 'host/attachment/put/open/project/host/attachment/put/open/project/host/attachment/put/open/project/uid?suid=$suid' | 否   | 附件上传地址。若不传则使用默认的附件服务器地址。                                     |
- 例子：
```js
yourSelfName.selectPic({
    maxCount:9
    }, (obj)=>{
        if(obj.resultCode==0){
            let list = obj.list
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    list:{}   // 上传附件成功后，服务器返回的原始内容
}
```
___
#### 拍摄并上传照片。该协议会返回上传成功后服务器返回的原始内容。：`yourSelfName.capturePic`
- 参数：

| 属性 | 类型   | 默认值  | 必填  | 说明                                                          |
|------|--------|------|:----|:------------------------------------------------------------|
| uploadUrl  | String | 'host/attachment/put/open/project/host/attachment/put/open/project/host/attachment/put/open/project/uid?suid=$suid' | 否   | 附件上传地址。若不传则使用默认的附件服务器地址。                                     |
- 例子：
```js
yourSelfName.capturePic({}, (obj)=>{
        if(obj.resultCode==0){
            let attachInfo = obj.attachInfo
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    attachInfo:{}   // 上传附件成功后，服务器返回的原始内容
}
```
___
#### 从相册选择并上传视频。该协议会返回上传成功后服务器返回的原始内容。目前仅可选择单个时长不超过5分钟的视频。：`yourSelfName.selectVideo`
- 参数：

| 属性 | 类型   | 默认值  | 必填  | 说明                                                          |
|------|--------|------|:----|:------------------------------------------------------------|
| uploadUrl  | String | 'host/attachment/put/open/project/host/attachment/put/open/project/host/attachment/put/open/project/uid?suid=$suid' | 否   | 附件上传地址。若不传则使用默认的附件服务器地址。                                     |
- 例子：
```js
yourSelfName.selectVideo({
        maxDuration:15
    }, (obj)=>{
        if(obj.resultCode==0){
            let attachInfo = obj.attachInfo
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    attachInfo:{}   // 上传附件成功后，服务器返回的原始内容
}
```
___
#### 拍摄并上传视频。该协议会返回上传成功后服务器返回的原始内容。：`yourSelfName.captureVideo`
- 参数：

| 属性 | 类型   | 默认值  | 必填  | 说明                                                          |
|------|--------|------|:----|:------------------------------------------------------------|
| maxDuration  | int |      | 是   | 最大录制时长，单位秒。                                     |
| uploadUrl  | String | 'host/attachment/put/open/project/host/attachment/put/open/project/host/attachment/put/open/project/uid?suid=$suid' | 否   | 附件上传地址。若不传则使用默认的附件服务器地址。                                     |
- 例子：
```js
yourSelfName.captureVideo({
    maxDuration:15
    }, (obj)=>{
        if(obj.resultCode==0){
            let attachInfo = obj.attachInfo
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    attachInfo:{}   // 上传附件成功后，服务器返回的原始内容
}
```
___
#### 设置屏幕亮度。：`yourSelfName.setBrightness`
- 参数：

| 属性 | 类型   | 默认值  | 必填  | 说明                                                          |
|------|--------|------|:----|:------------------------------------------------------------|
| brightness  | double |      | 是   | 屏幕亮度，取值在0.0-1.0之间                                     |
- 例子：
```js
yourSelfName.setBrightness({
        brightness:0.1
}, (obj)=>{
        if(obj.resultCode==0){
            console.log('操作成功')
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
}
```
___
#### 获取定位信息。：`yourSelfName.getLocation`

- 参数：无

- 例子：
```js
yourSelfName.getLocation((obj)=>{
    if(obj.resultCode==0){
        let longitude = obj.longitude;
        let latitude = obj.latitude;
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    longitude:104.06291,	// 经度
    latitude:30.67485		// 纬度
}
```
___
#### 获取应用当前状态快照（用户信息、孩子信息、机构信息、客户端类型）。：`yourSelfName.getAppSnapshot`

- 参数：无
- 例子：
```js
yourSelfName.getAppSnapshot(
    (obj)=>{
        if(obj.resultCode==0){
            let snapshot = obj.snapshot
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0,
    resultMsg:"", //错误信息
    snapshot:{
    user:{					// 用户信息
        id:0,				// 用户ID
            name:"",			// 用户名
            avatar:""			// 用户头像地址
    },
    kid:{					// 当前选中的孩子信息（仅家长端有效）
        id:0,				// 孩子ID
            name:"",			// 孩子名
            avatar:""			// 孩子头像地址
    },
    organization:{			// 当前选中机构信息
        id:0,				// 机构ID
            name:"",			// 机构名
            superiorId:0,		// 上级机构ID
            superiorName:"",	// 上级机构名
            groupType:"3",		// 机构类型
            memberType:"1"		// 在该机构下的身份类型
    },
    flavor:"parent"			// 渠道信息：家长端或教师端
    }
}

```
___
#### 调起指定应用。`yourSelfName.launchApp`

- 参数：

| 属性                 | 类型     | 默认值              | 必填  | 说明                                                         |
|:-------------------|:-------|:-----------------|-----|------------------------------------------------------------|
| androidPackageName | String |                  | 否   | Android端应用包名。|
| iOSUrlScheme       | String  |                  | 否   | iOS端应用声明的用于打开应用的Url Scheme。|
| appStoreLink       | String  |                  | 否   | iOS端应用在App Store上的地址。                             |


- 例子：
```js
yourSelfName.launchApp({
    androidPackageName:'',
    iOSUrlScheme:'',
    appStoreLink:'',
    }, (obj)=>{
        if(obj.resultCode==0){
            let status = obj.status
        }else{
            console.log(obj.resultMsg)
        }
    }
)
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
    status:0		// 调起结果。1 表成功调起应用；0 代表应用未安装，但已成功调起该应用所在的应用商店的页面。
}
```
___

#### h5调起系统相册、拍照、录像 `<input type='file'/>`
- 参数：

| 属性              | 类型     | 默认值     | 必填  | 说明                                                                                   |
|:----------------|:-------|:--------|-----|--------------------------------------------------------------------------------------|
| capture           |  |         | 否   | 用户未授权时，则会弹出系统相机、麦克风权限授权。拒绝后需在系统中选择打开授权，否则该属性将失效，只能选择系统文件                             |
| accept       | string  |         | 否   | image/* ｜video/* ，有capture时，分别为拍照和录像，不填默认为拍照，没有capture时，分别为选择照片及视频，不填则为选择所有系统文件、拍照及摄像 |
| multiple       |   |         | 否   | 有则可多选                                                                                |


- 例子：
```js
<div>多选视频/录像</div>
<input type="file" @change="fileChange($event)" accept="video/*" multiple>
<div>单选照片/拍照</div>
<input type="file" @change="fileChange($event)" accept="image/*">
<div>多选所有文件、拍照、录像</div>
<input type="file" @change="fileChange($event)" multiple>
<div>单选所有文件、拍照、录像</div>
<input type="file" @change="fileChange($event)">
<div>拍照</div>
<input type="file" @change="fileChange($event)" accept="image/*" capture>
<div>录像</div>
<input type="file" @change="fileChange($event)" accept="video/*" capture>
```
---
#### 分享至微信。：`yourSelfName.shareToWeChat`

- 参数：

| 属性              | 类型     | 默认值     | 必填  | 说明                                     |
|:----------------|:-------|:--------|-----|----------------------------------------|
| scene           | int |         | 是   | 待分享的场景，有效取值在1-3之间。1 代表朋友圈；2 代表会话；3 代表收藏。 |
| shareModel       | int  |         | 是   | 待分享的类型，有效取值在1-2之间。1 代表分享文本；2 代表分享网页。   |
| text       | String  |         | shareModel 为1时必填   | 待分享文本|
| webPage       |  String |         | shareModel 为2时必填   | 待分享网页地址                                  |
| title       | String  |         | shareModel 为2时必填   | 待分享网页标题                                  |
| description       | String  |         | shareModel 为2时必填   | 待分享网页描述                                  |
| thumbnail       |  String |         | shareModel 为2时必填   | 待分享网页封面                                  |

- 例子：
```js
yourSelfName.shareToWeChat({
    scene:2,
    shareModel:2,
    webPage:'XXX',
    title:'XXX',
    description:'XXX',
    thumbnail:'XXX',
},(obj)=>{
    if(obj.resultCode==0){
        console.log('操作成功')
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
}
```
___
#### 调起指定微信小程序。：`yourSelfName.launchMiniProgram`

- 参数：

| 属性 | 类型   | 默认值 | 必填  | 说明                                                                                                   |
|------|--------|--------|:----|:-----------------------------------------------------------------------------------------------------|
| id  | string |        | 是   | 待调起的小程序ID。|


- 例子：
```js
yourSelfName.launchMiniProgram({
    id:''
},(obj)=>{
    if(obj.resultCode==0){
        console.log('操作成功')
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
}
```
___
#### 支付。：`yourSelfName.submitPayOrder`

- 参数：

| 属性 | 类型   | 默认值 | 必填  | 说明                                                                                                       |
|------|------|--------|:----|:---------------------------------------------------------------------------------------------------------|
| channelId  | int |        | 是   | 支付渠道，有效取值在1-2之间。1 代表支付宝；2 代表微信。|
| channelParams  | String |        | 是   | 对应支付渠道的支付参数。|


- 例子：
```js
yourSelfName.submitPayOrder({
    channelId:1,
    channelParams:{}
},(obj)=>{
    if(obj.resultCode==0){
        console.log('支付成功')
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功 100：用户取消 101：用户取消支付 102：无支付客户端
    resultMsg:"", //错误信息
}
```
___
#### 设置Web页是否忽略页内导航栈。：`yourSelfName.ignoreHistory`

- 参数：

| 属性 | 类型   | 默认值  | 必填  | 说明                                                                                                       |
|------|------|------|:----|:---------------------------------------------------------------------------------------------------------|
| ignore  | bool | true | 是   | 是否忽略页内导航栈。true 忽略页内导航栈；false 不忽略页内导航栈。|

- 例子：
```js
yourSelfName.ignoreHistory({
    ignore:true,
},(obj)=>{
    if(obj.resultCode==0){
        console.log('操作成功')
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功
    resultMsg:"", //错误信息
}
```
___
#### 选取媒体文件。该协议不会对选取的文件进行上传，仅返回Web端可访问的本地文件地址。：`yourSelfName.chooseMedia`

- 参数：

| 属性 | 类型   | 默认值 | 必填  | 说明                                                                                                                         |
|------|------|--------|:----|:---------------------------------------------------------------------------------------------------------------------------|
| mediaType  | Array |        | 是   | 可选的媒体类型。支持 ['image', 'video'] 。'image' 代表图片；'video' 代表视频。                                                                                            |
| sourceType  | Array |        | 是   | 媒体选取的数据源。支持 ['album', 'camera'] 。'album' 代表从相册中选择；'camera' 代表使用摄像头选                                                                                                                           |
| count  | int |        | sourceType 包含 'album' 时必填   | 最多可选取的文件个数。                                                                                                                |
| maxDuration  | int |        | sourceType 包含 'camera' ，mediaType 包含 'video' 时必填   | 视频的最大录制时长，单位秒。不限制从相册选取的视频时长。                                                                                               |
| sizeType  | Array |        | 是   | 是否对媒体文件进行压缩。支持 ['original', 'compressed'] 。'original' 代表使用原始文件；'compressed' 代表对文件进行压缩处理。仅支持单个选项，当数组传入多个值时，以第一元素为准，剩余元素会被忽略。|
| camera  | String |        | sourceType 包含 'camera' 时必填   | 使用的摄像头类型。'back' 代表后置摄像头；'front' 代表前置摄像头。                                                                                   |


- 例子：
```js
yourSelfName.submitPayOrder({
    mediaType:['image'],
    sourceType:['album'],
    count:9,
    sizeType:['compressed'],
},(obj)=>{
    if(obj.resultCode==0){
        let list = obj.list
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功 100：用户取消 101：用户取消支付 102：无支付客户端
    resultMsg:"", //错误信息
    list:[
        {
            mimeType:"image/png",	// 媒体文件的mimeType
            data:"",				// 媒体文件在设备上的访问路径
        }
    ]
}
```
___
#### 上传媒体文件。该协议仅支持通过 chooseMedia 协议选取的媒体文件。：`yourSelfName.uploadMedia`

- 参数：

| 属性 | 类型     | 默认值 | 必填                   | 说明                                                                                                    |
|------|--------|--------|:---------------------|:------------------------------------------------------------------------------------------------------|
| url  | string |    host/attachment/put/open/project/uid?suid=$suid'| 否                    |        附件上传地址。若不传则使用默认的附件服务器地址。                                                                                               |
| filePath | String |        | 当不传 filePaths 时为必填参数 |                    单个媒体文件地址，地址需要确保是通过调用 chooseMedia 协议获取的。                                                                                   |
| filePaths | Array  |        | 当不传 filePath 时为必填参数  | 多个媒体文件地址列表，地址需要确保是通过调用 chooseMedia 协议获取的。                                                                                                      |
| fileName | String |        | 否                    |  上传单个媒体文件时的文件名                                                                                                     |
| fileNames | Array  |        | 否                    |  上传多个媒体文件时的文件名列表                                                                                                     |
| header  | Object |        | 否                    | 上传文件时的额外请求头。                                                                                          |
| formData | Object |        | 否                    | 上传文件时的额外表单数据                                                                                                      |


- 例子：
```js
yourSelfName.uploadMedia({
    filePath:'XXXX',
},(obj)=>{
    if(obj.resultCode==0){
        let data = obj.data;
    }else{
        console.log(obj.resultMsg)
    }
})
```
- 返回：
```js
{
    resultCode:0, //0为成功 100：用户取消 101：用户取消支付 102：无支付客户端
    resultMsg:"", //错误信息
    data:{}				// 上传附件成功后，服务器返回的原始内容
}
```
___

#### 人脸采集：`yourSelfName.faceCapture`

- 参数：

| 属性 | 类型     | 默认值    | 必填                   | 说明                                                                                                                                            |
|------|--------|--------|:---------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| camera  | string | 'back' | 否                    | 初始摄像头类型。'back' 代表后置摄像头；'front' 代表前置摄像头。                                                                                                                                              |
| startTip | String | ''     | 否 | 头部提醒文案。出现在采集页面的左上角（LTR）或右上角（RTL）。                                                                                                       |
| endTip | String  | ''     | 否  | 尾部提醒文案。出现在采集页面的右上角（LTR）或左上角（RTL）。                                                                                                    |
| wellPositionThreshold | int | 5      | 否                    | 人脸有效位置的检测阈值，该阈值用于检测人脸是否居中，是否平视摄像头。值越大，检测的宽容度越高；值越小，检测的宽容度约低。                                                                                                                                 |
| eyesOpenCheck | bool  | true   | 否                    | 是有启用睁眼检测                                                                                                                              |

- 例子：

```js
yourSelfName.faceCapture({
    startTip: 'XXXX',
    endTip: 'XXXX',
}, (obj) => {
    if (obj.resultCode == 0) {
        let data = obj.data;
    } else {
        console.log(obj.resultMsg)
    }
})
```

- 返回：

```js
{
    resultCode:0, //0为成功 100：用户取消 
    resultMsg:"", //错误信息
    image:""	// 人脸图片文件在设备上的访问路径,可直接放在img标签中使用，上传则需要调用yourSelfName.uploadMedia方法
}
```

___
