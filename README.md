### 使用说明
- 引用npm包

```
npm i -D nativetoh5
```

- 使用npm包

```js
import {getNativeToH5} from 'nativetoh5'
```

- 初始化协议对象

```js
getNativeToH5().then((res)=>{
    let yourSelfName = new res();
    yourSelfName.init();
    console.log(yourSelfName.name);
    console.log(yourSelfName.version);
})
```
___
### 协议说明
[点击查看](https://gitee.com/zhixue-edu/ischool-jssdk/blob/main/explain.md)