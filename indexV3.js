let NativeToH5 = null;

//判断环境
function getPlatform(_platform) {
    if (_platform === 'wxmp') {    //微信
        return Promise.resolve({
            name: _platform,
            version: wx.getSystemInfoSync().SDKVersion,
        });
    } else if (_platform === 'ryt') { //融E通
        return Promise.resolve({
            name: 'ryt',
            version: '1.0.0',
        });
    } else {
        let ua = navigator ? navigator.userAgent : "";
        //ua = 'Mozilla/5.0 (Linux; Android 12; M2102K1AC Build/SKQ1.211006.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/95.0.4638.74 Mobile Safari/537.36 wyczc cjt-android-1.0.0 (cjt/M2102K1AC/12)'
        //ua = 'ischool-ios-9.6.1 (teacher/iPhone14,3/16.2)\\Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_3 like Mac OS X) AppleWebKit/603.3.8 (KHTML, like Gecko) Mobile/14G60'
        let name = '';
        let version = '';
        let oldAppReg = /ischool\-(\w+)\-([\d\.]+)(?:-([\d\.]+))?/;
        let oldAppMatch = ua.match(oldAppReg);
        if (oldAppMatch) {   //v9
            return Promise.resolve({
                name: 'oldApp',
                version: oldAppMatch[3] || oldAppMatch[2],
            });
        } else if (ua.indexOf('wyczc') > -1) {  //v10
            let uu = ua.split('wyczc')[1].trim().split(' ')[0];
            name = uu.split('-')[0];
            version = uu.split('-')[2];
            let timer1 = null;
            let timer2 = null;
            return new Promise((resolve, reject) => {
                timer1 = setInterval(() => {
                    if (window.platform_app) {
                        clearInterval(timer1);
                        clearInterval(timer2);
                        resolve();
                    }
                }, 50)
                timer2 = setTimeout(() => {
                    clearInterval(timer1);
                    clearInterval(timer2);
                    reject();
                }, 500)
            }).then(() => {
                return Promise.resolve({
                    name: name,
                    version: version,
                });
            }).catch(() => {
                console.log('环境获取超时')
                return Promise.resolve({
                    name: '',
                    version: '',
                });
            })
        } else if (ua.indexOf('ryt') > -1) { //融E通
            return Promise.resolve({
                name: 'ryt',
                version: '1.0.0',
            });
        } else {    //未知环境
            return Promise.resolve({
                name: '',
                version: '',
            });
        }
    }
}

//环境
async function getNativeToH5(_platform) {
    let platform = await getPlatform(_platform);
    if (platform.name === 'wxmp') {
        NativeToH5 = require('./src/nativeWX.js').NativeToH5;
    } else if (platform.name === 'oldApp') {
        NativeToH5 = require('./src/native.js').NativeToH5;
    } else if (platform.name === 'ryt') {
        NativeToH5 = require('./src/nativeRYT.js').NativeToH5;
    } else if (platform.name) {
        NativeToH5 = require('./src/nativeV2.js').NativeToH5;
    } else {
        console.log('协议未在相应应用中调用')
        NativeToH5 = require('./src/native.js').NativeToH5;
    }
    NativeToH5.prototype.name = platform.name;
    NativeToH5.prototype.version = platform.version;
    return NativeToH5
}

export {getNativeToH5}
